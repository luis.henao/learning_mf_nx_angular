# shared-util-mf

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-util-mf` to execute the unit tests via [Jest](https://jestjs.io).

## Running lint

Run `nx lint shared-util-mf` to execute the lint via [ESLint](https://eslint.org/).
