export interface ExposeModulesDefinition {
  [key: string]: {
    path: string;
    className: string;
    presentationalLayer?: string;
  };
}

export interface RemoteDefinition {
  name: string;
  remote: string;
  type: string;
  exposes: ExposeModulesDefinition;
}

/**
 * El tipo de definición component hace referencia a un componente remoto que se intancia como un
 * elemento definido dentro arquitectura como capa presentacional del Kernel
 *
 * El tipo de definición plugin hace referencia a un módulo remoto que será cargado desde la capa presentacional
 *
 * El tipo de definición kernel hace referencia a un módulo remoto que será cargado desde y como extensíón
 * de la funcionalidad del kernel. <<<Este se en encuentra en proceso de revisión.>>>
 */
export type TypeFederatedModuleDefinition = 'component' | 'plugin' | 'kernel';

export interface FederatedModuleDefinitions {
  [key: string]: {
    remote: string;
    type: TypeFederatedModuleDefinition;
  };
}

let federatedModuleDefinitions: FederatedModuleDefinitions;
export function setFederatedModuleDefinitions(definitions: FederatedModuleDefinitions): void {
  federatedModuleDefinitions = definitions;
}

export function getFederatedModuleDefinitions() {
  return federatedModuleDefinitions;
}

let remoteModuleDefinitions: Map<string, RemoteDefinition> = new Map<string, RemoteDefinition>();
export function setRemoteModuleDefinition(definition: RemoteDefinition): void {
  if (remoteModuleDefinitions.has(definition.name)) {
    throw new Error(`La definición de módulo remoto ${definition.name} ya ha sido registrada`);
  }
  remoteModuleDefinitions.set(definition.name, definition);
}

export function getRemoteModuleDefinition() {
  return remoteModuleDefinitions;
}
