import { setRemoteDefinitions } from '@nrwl/angular/mfe';
import { load } from 'js-yaml';

import {
  FederatedModuleDefinitions,
  RemoteDefinition,
  setFederatedModuleDefinitions,
  setRemoteModuleDefinition,
} from '@qx-test/shared/util-mf';

fetch('./assets/module-federation.manifest.yml')
  .then((res) => res.text())
  .then(async (definitions) => {
    const parsedDefinitions = load(definitions) as FederatedModuleDefinitions;
    const remoteDefinitions = await (await fetch(parsedDefinitions['plugin-header'].remote)).text();
    const parsedRemoteDefinition = load(remoteDefinitions) as RemoteDefinition;

    setFederatedModuleDefinitions(parsedDefinitions);
    setRemoteModuleDefinition(parsedRemoteDefinition);

    setRemoteDefinitions({ 'plugin-header': parsedRemoteDefinition.remote });
    // Crear util para mantener en memoria todas las definiciones de módulos federados
    // Crear util para cargar todas las definiciones remotas que son de tipo component o kernel
    // Crear util para mantener en memoria todas las definiciones remotas descargadas
    //setRemoteDefinitions(load(definitions) as Record<string, string>)
  })
  .then(() => import('./bootstrap').catch((err) => console.error(err)));
