import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {loadRemoteModule} from "@nrwl/angular/mfe";

@Component({
  selector: 'qx-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent  implements OnInit {
  @ViewChild('pluginHeader', {read: ViewContainerRef, static: true}) private pluginHeader!: ViewContainerRef;
  title = 'kernel';

  async ngOnInit() {
    /**
     * Es un antipatrón permitir que un componente de un plugin dado comparta sus servicios con
     * otros plugin. Todo debe partir y pasar por el kernel. ¿Hay forma de extender los servicios del
     * kernel de forma dinámica con otro módulo remoto? (Esto tiene algo en contra y es que ese módulo remoto
     * funciona como un plugin, lo cual estaría creando el antipatron)
     */
    const component = await loadRemoteModule('plugin-header', 'RemoteComponent').then(
        (m) => {
          return m.RemoteEntryComponent;
        }
      );
    console.log(component);
    this.pluginHeader.createComponent(component);
  }
}
