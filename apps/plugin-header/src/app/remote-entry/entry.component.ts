import {Component, OnInit} from '@angular/core';
import { getRemoteModuleDefinition } from '@qx-test/shared/util-mf';

@Component({
  selector: 'plugin-header-entry',
  template: `<div class="remote-entry">
    <h2>plugin-header's Remote Entry Component WOO!</h2>
  </div>`,
  styles: [
    `
      .remote-entry {
        background-color: #143055;
        color: white;
        padding: 5px;
      }
    `,
  ],
})
export class RemoteEntryComponent implements OnInit{
  ngOnInit() {
    console.log(getRemoteModuleDefinition());
  }
}
