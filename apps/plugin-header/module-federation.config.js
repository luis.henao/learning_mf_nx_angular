const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');

const filename = path.join(__dirname, 'src/assets/config/plugin.manifest.yml');
const pluginManifest = fs.readFileSync(filename, 'utf8');
let { name, exposes } = yaml.load(pluginManifest);
let exposesEntries = Object.keys(exposes).map((key) => [key, exposes[key].path]);

module.exports = {
  name,
  exposes: Object.fromEntries(exposesEntries),
};

